package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import selenium.WebPage;

import static org.junit.Assert.*;

public class Stepdefs {
	private WebPage page = new WebPage();
	
	@Given("I launch Chrome browser")
	public void i_launch_Chrome_browser() {
		page.launchBrowser();
	}

	@When("I open http:\\/\\/thedemosite.co.uk\\/login.php")
	public void i_open_http_thedemosite_co_uk_login_php() {
		page.openDemoURL();
	}

	@Then("I verify that the page displays login form")
	public void i_verify_that_the_page_displays_login_form() {
		page.checkLoginFormIsDisplayed();
	}

	@When("I try to login using test for username and password")
	public void i_try_to_login_using_test_for_username_and_password() {
		page.loginWithUsernameAndPassword();
	}

	@Then("Login failed")
	public void login_failed() {
		page.checkLoginFailed();
	}
}
