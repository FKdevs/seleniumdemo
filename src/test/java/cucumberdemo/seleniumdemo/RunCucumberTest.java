package cucumberdemo.seleniumdemo;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = "stepdefinitions", plugin = { "pretty",
		"json:target/report.json" })
public class RunCucumberTest {
}
