package selenium;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebPage {
	public static WebDriver driver;
	
	public void launchBrowser() {
		driver = new ChromeDriver();
	}
	
	public void openDemoURL() {
		driver.get("http://thedemosite.co.uk/login.php");
	}
		
	public void checkLoginFormIsDisplayed() {
		assertTrue(driver.findElement(By.name("saveform")).isDisplayed());
	}
	
	public void loginWithUsernameAndPassword() {
		driver.findElement(By.name("username")).sendKeys("test");
	    driver.findElement(By.name("password")).sendKeys("test");     
	}
	
	public void checkLoginFailed() {
	    driver.findElement(By.name("FormsButton2")).click();
	    assertTrue(driver.findElement(By.tagName("blockquote")).getText().equals("**Failed Login**"));
	}
}
