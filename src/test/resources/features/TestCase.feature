Feature: http://thedemosite.co.uk/login.php
This feature verifies the login functionality on http://thedemosite.co.uk/login.php
 
Scenario: Check that login form on http://thedemosite.co.uk/login.php
Given I launch Chrome browser
When I open http://thedemosite.co.uk/login.php
Then I verify that the page displays login form
When I try to login using test for username and password
Then Login failed